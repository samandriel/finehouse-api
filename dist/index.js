'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

// Database Connection
_mongoose2.default.connect(process.env.DB_URI, { useMongoClient: true });
_mongoose2.default.Promise = global.Promise;

// Middlewares
var corsOptions = {
  origin: 'http://localhost:8080'
};
app.use((0, _cors2.default)(corsOptions));
app.use((0, _morgan2.default)('dev'));
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use(_bodyParser2.default.json());
app.use(_passport2.default.initialize());
app.use('/', _routes2.default);
app.use(function (err, req, res, next) {
  res.status(422).send({ error: err.message });
});

// Start server
var port = process.env.PORT || 4000;
app.listen(port, function () {
  console.log('listening for requests on port ' + port);
});
//# sourceMappingURL=index.js.map
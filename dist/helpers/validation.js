'use strict';

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  validate: function validate(schema) {
    return function (req, res, next) {
      var result = _joi2.default.validate(req.body, schema);
      if (result.error) {
        return res.status(400).json(result.error);
      }

      if (!req.value) {
        req.value = {};
      }
      req.value['body'] = result.value;

      next();
    };
  },

  schema: {
    choreSchema: _joi2.default.object().keys({
      name: _joi2.default.string().required(),
      description: _joi2.default.string()
    }),
    // userRegistrationSchema: Joi.object().keys({
    //   profile: Joi.object().keys({
    //     name: Joi.object().keys({
    //       givenName: Joi.string().required(),
    //       familyName: Joi.string().required()
    //     })
    //   }),
    //   email: Joi.string().email().trim(),
    //   password: Joi.string().strip()
    // }),
    userAuthenticationSchema: _joi2.default.object().keys({
      email: _joi2.default.string().email(),
      password: _joi2.default.string().strip()
    })
  }
};
//# sourceMappingURL=validation.js.map
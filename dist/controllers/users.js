'use strict';

var existingAccount = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
    var existingAccount;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            existingAccount = void 0;
            _context.prev = 1;
            _context.next = 4;
            return _user2.default.find({}).or([{ 'google.email': req.body.google.email }, { 'facebook.email': req.body.google.email }, { 'email': req.body.google.email }]);

          case 4:
            existingAccount = _context.sent;

            existingAccount = existingAccount[0];
            // Check if the another account with the same authentication method is already been in use
            if (existingAccount) {
              if (existingAccount.google.email && existingAccount.google.email !== null) {
                res.status(422).json({ error: 'You already connected with another Google Account: ' + existingAccount.google.email + ', please use this account to login and update your profile' });
              } else {
                existingAccount.update(req.body).exec();
              }
            }
            _context.next = 34;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context['catch'](1);
            _context.prev = 11;
            _context.next = 14;
            return _user2.default.find({}).or([{ 'google.email': req.body.facebook.email }, { 'facebook.email': req.body.facebook.email }, { 'email': req.body.facebook.email }]);

          case 14:
            existingAccount = _context.sent;

            existingAccount = existingAccount[0];
            console.log(existingAccount);
            // Check if the another account with the same authentication method is already been in use
            if (existingAccount) {
              if (existingAccount.facebook.email && existingAccount.facebook.email !== null) {
                res.status(422).json({ error: 'You already connected with another Facebook Account: ' + existingAccount.facebook.email + ', please use this account to login and update your profile' });
              } else {
                existingAccount.update(req.body).exec();
              }
            }
            _context.next = 34;
            break;

          case 20:
            _context.prev = 20;
            _context.t1 = _context['catch'](11);
            _context.prev = 22;
            _context.next = 25;
            return _user2.default.find({}).or([{ 'google.email': req.body.email }, { 'facebook.email': req.body.email }, { email: req.body.email }]);

          case 25:
            existingAccount = _context.sent;

            existingAccount = existingAccount[0];
            // Check if the another account with the same authentication method is already been in use
            console.log(existingAccount);
            if (existingAccount) {
              if (existingAccount.email && existingAccount.email !== null) {
                res.status(422).json({ error: 'You already have an account ' + existingAccount.email });
              } else {
                existingAccount.update(req.body).exec();
              }
            }
            _context.next = 34;
            break;

          case 31:
            _context.prev = 31;
            _context.t2 = _context['catch'](22);

            next(_context.t2);

          case 34:
            return _context.abrupt('return', existingAccount);

          case 35:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[1, 9], [11, 20], [22, 31]]);
  }));

  return function existingAccount(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _user = require('./../models/user');

var _user2 = _interopRequireDefault(_user);

var _family = require('./../models/family');

var _family2 = _interopRequireDefault(_family);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function authToken(user) {
  return _jsonwebtoken2.default.sign({
    iss: 'http://www.finehouse.co/',
    sub: user.id,
    exp: Date.now() + 7
  }, process.env.JWT_SECRET, { algorithm: 'HS512' });
}

module.exports = {

  registration: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
      var user, family, token;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.prev = 0;
              user = void 0;
              family = void 0;
              // First user?

              _user2.default.findOne().exec(function () {
                var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(err, user) {
                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          if (!err) {
                            _context2.next = 2;
                            break;
                          }

                          throw err;

                        case 2:
                          if (user) {
                            _context2.next = 9;
                            break;
                          }

                          _context2.next = 5;
                          return _user2.default.create(req.body);

                        case 5:
                          user = _context2.sent;
                          _context2.next = 8;
                          return _family2.default.create({ familyName: '', members: [user._id] });

                        case 8:
                          family = _context2.sent;

                        case 9:
                        case 'end':
                          return _context2.stop();
                      }
                    }
                  }, _callee2, undefined);
                }));

                return function (_x7, _x8) {
                  return _ref3.apply(this, arguments);
                };
              }());

              // Normal operation
              _context3.next = 6;
              return existingAccount(req, res, next);

            case 6:
              user = _context3.sent;

              if (!user) {
                _context3.next = 14;
                break;
              }

              _context3.next = 10;
              return _family2.default.find({ members: user._id });

            case 10:
              family = _context3.sent;

              console.log(JSON.stringify(family));
              _context3.next = 34;
              break;

            case 14:
              _context3.next = 16;
              return _user2.default.create(req.body);

            case 16:
              user = _context3.sent;
              _context3.prev = 17;
              _context3.next = 20;
              return _family2.default.create({ familyName: user.profile.name.familyName, members: [user._id] });

            case 20:
              family = _context3.sent;
              _context3.next = 34;
              break;

            case 23:
              _context3.prev = 23;
              _context3.t0 = _context3['catch'](17);
              _context3.prev = 25;
              _context3.next = 28;
              return _family2.default.create({ familyName: '', members: [user._id] });

            case 28:
              family = _context3.sent;
              _context3.next = 34;
              break;

            case 31:
              _context3.prev = 31;
              _context3.t1 = _context3['catch'](25);

              next();

            case 34:
              _context3.next = 36;
              return authToken(user);

            case 36:
              token = _context3.sent;
              _context3.next = 39;
              return res.status(200).json({ token: token, family: family });

            case 39:
              _context3.next = 45;
              break;

            case 41:
              _context3.prev = 41;
              _context3.t2 = _context3['catch'](0);
              _context3.next = 45;
              return next(_context3.t2);

            case 45:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined, [[0, 41], [17, 23], [25, 31]]);
    }));

    return function registration(_x4, _x5, _x6) {
      return _ref2.apply(this, arguments);
    };
  }(),

  authentication: function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res, next) {
      var token;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return authToken(req.user);

            case 2:
              token = _context4.sent;

              res.status(200).json({ token: token });
              next();

            case 5:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, undefined);
    }));

    return function authentication(_x9, _x10, _x11) {
      return _ref4.apply(this, arguments);
    };
  }()
};
//# sourceMappingURL=users.js.map
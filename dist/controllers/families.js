'use strict';

var _family = require('./../models/family');

var _family2 = _interopRequireDefault(_family);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

module.exports = {
  // create: async (req, res, next) => {
  //   try {
  //     await
  //     Family.create(req.body).then((family) => {
  //       res.send(family)
  //     })
  //   } catch (error) {
  //     error(next)
  //   }
  // },
  read: function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
      var family;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;

              console.log(res);
              _context.next = 4;
              return _family2.default.findOne({ members: { $in: [req.user._id] } });

            case 4:
              family = _context.sent;

              res.status(200).json({ family: family });
              _context.next = 11;
              break;

            case 8:
              _context.prev = 8;
              _context.t0 = _context['catch'](0);

              _context.t0(next);

            case 11:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined, [[0, 8]]);
    }));

    return function read(_x, _x2, _x3) {
      return _ref.apply(this, arguments);
    };
  }(),

  // update: async (req, res, next) => {
  //   try {
  //     let family = await Family.findOne({ _id: req.body.family._id })
  //     family.update(res.body.family)

  //     await
  //     res.send({type: 'GET'})
  //   } catch (error) {
  //     error(next)
  //   }
  // },

  destroy: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res, next) {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              _context2.next = 3;
              return res.send({ type: 'GET' });

            case 3:
              _context2.next = 8;
              break;

            case 5:
              _context2.prev = 5;
              _context2.t0 = _context2['catch'](0);

              _context2.t0(next);

            case 8:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined, [[0, 5]]);
    }));

    return function destroy(_x4, _x5, _x6) {
      return _ref2.apply(this, arguments);
    };
  }()
};
//# sourceMappingURL=families.js.map
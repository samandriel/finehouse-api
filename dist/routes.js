'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

require('./configs/passport');

var _families = require('./controllers/families');

var _families2 = _interopRequireDefault(_families);

var _chorePlans = require('./controllers/chore-plans');

var _chorePlans2 = _interopRequireDefault(_chorePlans);

var _reminders = require('./controllers/reminders');

var _reminders2 = _interopRequireDefault(_reminders);

var _billTrackers = require('./controllers/bill-trackers');

var _billTrackers2 = _interopRequireDefault(_billTrackers);

var _users = require('./controllers/users');

var _users2 = _interopRequireDefault(_users);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import ChoresController from './controllers/chores'

// import { validate, schema } from './helpers/validation'
var router = _express2.default.Router();
var userAuthorization = _passport2.default.authenticate('jwt', { session: false });
var localAuthentication = _passport2.default.authenticate('local', { session: false, failureRedirect: '/user/auth' });

// Auth
router.post('/user/registration', _users2.default.registration);
router.post('/user/auth', localAuthentication, _users2.default.authentication);
router.get('/user/auth/google', _passport2.default.authenticate('google', { scope: ['profile', 'email'] }));
router.get('/user/auth/google/callback', _passport2.default.authenticate('google', { session: false, failureRedirect: '/user/auth/google' }), _users2.default.authentication);
router.get('/user/auth/facebook', _passport2.default.authenticate('facebook', { scope: ['public_profile', 'email'] }));
router.get('/user/auth/facebook/callback', _passport2.default.authenticate('facebook', { session: false, failureRedirect: '/user/auth/facebook' }), _users2.default.authentication);

// router.get('/family', userAuthorization, ChorePlansController.index)
// router.post('/families', userAuthorization, FamilyController.create)
router.get('/family/:id', userAuthorization, _families2.default.read);
// router.patch('/family/:id', userAuthorization, FamilyController.update)
router.delete('/family/:id', userAuthorization, _families2.default.destroy);

// router.get('/chores', userAuthorization, ChoresController.index)
// router.post('/chores', userAuthorization, validate(schema.choreSchema), ChoresController.create)
// router.get('/chore/:id', userAuthorization, ChoresController.read)
// router.put('/chore/:id', userAuthorization, ChoresController.update)
// router.delete('/chore/:id', userAuthorization, ChoresController.destroy)

router.get('/chore_plans', userAuthorization, _chorePlans2.default.index);
router.post('/chore_plans', userAuthorization, _chorePlans2.default.create);
router.get('/chore_plan/:id', userAuthorization, _chorePlans2.default.read);
router.patch('/chore_plan/:id', userAuthorization, _chorePlans2.default.update);
router.delete('/chore_plan/:id', userAuthorization, _chorePlans2.default.destroy);

router.get('/reminders', userAuthorization, _reminders2.default.index);
router.post('/reminders', userAuthorization, _reminders2.default.create);
router.get('/reminder/:id', userAuthorization, _reminders2.default.read);
router.patch('/reminder/:id', userAuthorization, _reminders2.default.update);
router.delete('/reminder/:id', userAuthorization, _reminders2.default.destroy);

router.get('/bill-trackers', userAuthorization, _billTrackers2.default.index);
router.post('/bill-trackers', userAuthorization, _billTrackers2.default.create);
router.get('/bill-tracker/:id', userAuthorization, _billTrackers2.default.read);
router.patch('/bill-tracker/:id', userAuthorization, _billTrackers2.default.update);
router.delete('/bill-tracker/:id', userAuthorization, _billTrackers2.default.destroy);

module.exports = router;
//# sourceMappingURL=routes.js.map
'use strict';

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportJwt = require('passport-jwt');

var _passportFacebook = require('passport-facebook');

var _passportGoogleOauth = require('passport-google-oauth');

var _passportLocal = require('passport-local');

var _user = require('./../models/user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

_passport2.default.use(new _passportJwt.Strategy({
  jwtFromRequest: _passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
  algorithm: 'HS512',
  issuer: 'http://www.finehouse.co/'
}, function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(payload, done) {
    var user;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _user2.default.findById(payload.sub);

          case 3:
            user = _context.sent;

            if (user) {
              _context.next = 6;
              break;
            }

            return _context.abrupt('return', done(null, false));

          case 6:
            return _context.abrupt('return', done(null, user));

          case 9:
            _context.prev = 9;
            _context.t0 = _context['catch'](0);

            done(_context.t0, false);

          case 12:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 9]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}()));

_passport2.default.use(new _passportLocal.Strategy({
  usernameField: 'email'
}, function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(email, password, done) {
    var user;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return _user2.default.findOne({ email: email });

          case 3:
            user = _context2.sent;

            if (user) {
              _context2.next = 6;
              break;
            }

            return _context2.abrupt('return', done(null, false));

          case 6:
            _context2.next = 8;
            return user.comparePassword(password, function (error, isMatch) {
              if (error) throw error;
              if (!isMatch) {
                return done(null, false);
              }
              done(null, user);
            });

          case 8:
            _context2.next = 13;
            break;

          case 10:
            _context2.prev = 10;
            _context2.t0 = _context2['catch'](0);

            done(_context2.t0, false);

          case 13:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined, [[0, 10]]);
  }));

  return function (_x3, _x4, _x5) {
    return _ref2.apply(this, arguments);
  };
}()));

_passport2.default.use(new _passportGoogleOauth.OAuth2Strategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.GOOGLE_CALLBACK_URL
}, function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(accessToken, refreshToken, profile, done) {
    var user;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _user2.default.findOne({ 'google.id': profile.id });

          case 3:
            user = _context3.sent;

            if (user) {
              _context3.next = 15;
              break;
            }

            user = new _user2.default();
            user.google.id = profile.id;
            user.google.token = accessToken;
            user.google.email = profile.emails[0].value;
            user.name.givenName = profile.name.givenName;
            user.name.familyName = profile.name.familyName;
            user.email = profile.emails[0].value;
            _context3.next = 14;
            return user.save();

          case 14:
            console.log(user);

          case 15:
            return _context3.abrupt('return', done(null, user));

          case 18:
            _context3.prev = 18;
            _context3.t0 = _context3['catch'](0);

            done(_context3.t0, false);

          case 21:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined, [[0, 18]]);
  }));

  return function (_x6, _x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
}()));

_passport2.default.use(new _passportFacebook.Strategy({
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL,
  profileFields: ['id', 'name', 'picture', 'email']
}, function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(accessToken, refreshToken, profile, done) {
    var user;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _user2.default.findOne({ 'facebook.id': profile.id });

          case 3:
            user = _context4.sent;

            if (user) {
              _context4.next = 15;
              break;
            }

            console.log(profile);
            user = new _user2.default();
            user.facebook.id = profile.id;
            user.facebook.token = accessToken;
            user.facebook.emails = profile.emails[0].value;
            user.name.givenName = profile.name.givenName;
            user.name.familyName = profile.name.familyName;
            user.email = profile.emails[0].value;
            _context4.next = 15;
            return user.save();

          case 15:
            return _context4.abrupt('return', done(null, user));

          case 18:
            _context4.prev = 18;
            _context4.t0 = _context4['catch'](0);

            done(_context4.t0, false);

          case 21:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, undefined, [[0, 18]]);
  }));

  return function (_x10, _x11, _x12, _x13) {
    return _ref4.apply(this, arguments);
  };
}()));
//# sourceMappingURL=passport.js.map
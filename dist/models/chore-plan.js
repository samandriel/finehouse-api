'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChorePlanSchema = new _mongoose.Schema({
  name: {
    type: String,
    unique: true
  },
  days: {
    monday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    tuesday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    wednesday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    thursday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    friday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    saturday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    sunday: [{
      chore_id: {
        type: _mongoose.Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: _mongoose.Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }]
  },
  created_by: _mongoose.Schema.ObjectId
});

var ChorePlan = _mongoose2.default.model('ChorePlan', ChorePlanSchema);
exports.default = ChorePlan;
//# sourceMappingURL=chore-plan.js.map
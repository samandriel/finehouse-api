'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChoreSchema = new _mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  created_by: _mongoose.Schema.ObjectID
});

var Chore = _mongoose2.default.model('Chore', ChoreSchema);
exports.default = Chore;
//# sourceMappingURL=chore.js.map
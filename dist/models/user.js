'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bcryptjs = require('bcryptjs');

var _bcryptjs2 = _interopRequireDefault(_bcryptjs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var UserSchema = new _mongoose.Schema({
  profile: {
    name: {
      givenName: {
        type: String,
        trim: true
      },
      familyName: {
        type: String,
        trim: true
      }
    },
    birthday: {
      type: Date
    },
    picture: {
      type: String
    }
  },
  email: {
    type: String,
    trim: true
  },
  password: String,
  google: {
    id: {
      type: String
    },
    token: {
      type: String
    },
    email: {
      type: String
    }
  },
  facebook: {
    id: {
      type: String
    },
    token: {
      type: String
    },
    email: {
      type: String
    }
  }
});

UserSchema.pre('save', function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(next) {
    var salt, hashedPassword;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (!(this.password != null)) {
              _context.next = 9;
              break;
            }

            _context.next = 4;
            return _bcryptjs2.default.genSalt(10);

          case 4:
            salt = _context.sent;
            _context.next = 7;
            return _bcryptjs2.default.hash(this.password, salt);

          case 7:
            hashedPassword = _context.sent;

            this.password = hashedPassword;

          case 9:
            next();
            _context.next = 15;
            break;

          case 12:
            _context.prev = 12;
            _context.t0 = _context['catch'](0);

            next(_context.t0);

          case 15:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 12]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}());

UserSchema.methods.comparePassword = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(inputPassword, next) {
    var isMatch;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return _bcryptjs2.default.compare(inputPassword, this.password);

          case 3:
            isMatch = _context2.sent;

            next(null, isMatch);
            _context2.next = 10;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2['catch'](0);

            next(_context2.t0, false);

          case 10:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this, [[0, 7]]);
  }));

  return function (_x2, _x3) {
    return _ref2.apply(this, arguments);
  };
}();

var User = _mongoose2.default.model('User', UserSchema);
exports.default = User;
//# sourceMappingURL=user.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ReminderSchema = new _mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  description: String,
  reminderTime: Date,
  assignedTo: [{
    type: _mongoose.Schema.ObjectId,
    ref: 'User'
  }],
  checklist: [{
    listItem: String,
    checked: {
      type: Boolean,
      default: false
    }
  }],
  private: {
    type: Boolean,
    defalse: false
  },
  created_by: _mongoose.Schema.ObjectId
});

var Reminder = _mongoose2.default.model('Reminder', ReminderSchema);
exports.default = Reminder;
//# sourceMappingURL=reminder.js.map
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BillTrackerSchema = new _mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  billCycle: {
    startDate: Number,
    startMonth: Number,
    recurring_payment: String
  },
  reminderTime: Date,
  assignedTo: [{
    type: _mongoose.Schema.ObjectId,
    ref: 'User'
  }],
  created_by: _mongoose.Schema.ObjectId
});

var BillTracker = _mongoose2.default.model('BillTracker', BillTrackerSchema);
exports.default = BillTracker;
//# sourceMappingURL=bill-tracker.js.map
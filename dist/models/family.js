'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FamilySchema = new _mongoose.Schema({
  familyName: {
    type: String
  },
  members: [_mongoose.Schema.ObjectId]
});

var Family = _mongoose2.default.model('family', FamilySchema);
exports.default = Family;
//# sourceMappingURL=family.js.map
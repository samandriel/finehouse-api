import express from 'express'
import passport from 'passport'
// import { validate, schema } from './helpers/validation'
import './configs/passport'

import FamilyController from './controllers/families'
// import ChoresController from './controllers/chores'
import ChorePlansController from './controllers/chore-plans'
import RemindersController from './controllers/reminders'
import BillTraclersController from './controllers/bill-trackers'
import UsersController from './controllers/users'

const router = express.Router()
const userAuthorization = passport.authenticate('jwt', { session: false })
const localAuthentication = passport.authenticate('local', { session: false, failureRedirect: '/user/auth' })

// Auth
router.post('/user/registration', UsersController.registration)
router.post('/user/auth', localAuthentication, UsersController.authentication)
router.get('/user/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }))
router.get('/user/auth/google/callback', passport.authenticate('google', { session: false, failureRedirect: '/user/auth/google' }), UsersController.authentication)
router.get('/user/auth/facebook', passport.authenticate('facebook', { scope: ['public_profile', 'email'] }))
router.get('/user/auth/facebook/callback', passport.authenticate('facebook', { session: false, failureRedirect: '/user/auth/facebook' }), UsersController.authentication)

// router.get('/family', userAuthorization, ChorePlansController.index)
// router.post('/families', userAuthorization, FamilyController.create)
router.get('/family/:id', userAuthorization, FamilyController.read)
// router.patch('/family/:id', userAuthorization, FamilyController.update)
router.delete('/family/:id', userAuthorization, FamilyController.destroy)

// router.get('/chores', userAuthorization, ChoresController.index)
// router.post('/chores', userAuthorization, validate(schema.choreSchema), ChoresController.create)
// router.get('/chore/:id', userAuthorization, ChoresController.read)
// router.put('/chore/:id', userAuthorization, ChoresController.update)
// router.delete('/chore/:id', userAuthorization, ChoresController.destroy)

router.get('/chore_plans', userAuthorization, ChorePlansController.index)
router.post('/chore_plans', userAuthorization, ChorePlansController.create)
router.get('/chore_plan/:id', userAuthorization, ChorePlansController.read)
router.patch('/chore_plan/:id', userAuthorization, ChorePlansController.update)
router.delete('/chore_plan/:id', userAuthorization, ChorePlansController.destroy)

router.get('/reminders', userAuthorization, RemindersController.index)
router.post('/reminders', userAuthorization, RemindersController.create)
router.get('/reminder/:id', userAuthorization, RemindersController.read)
router.patch('/reminder/:id', userAuthorization, RemindersController.update)
router.delete('/reminder/:id', userAuthorization, RemindersController.destroy)

router.get('/bill-trackers', userAuthorization, BillTraclersController.index)
router.post('/bill-trackers', userAuthorization, BillTraclersController.create)
router.get('/bill-tracker/:id', userAuthorization, BillTraclersController.read)
router.patch('/bill-tracker/:id', userAuthorization, BillTraclersController.update)
router.delete('/bill-tracker/:id', userAuthorization, BillTraclersController.destroy)

module.exports = router

import Joi from 'joi'

module.exports = {
  validate: (schema) => {
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema)
      if (result.error) {
        return res.status(400).json(result.error)
      }

      if (!req.value) { req.value = {} }
      req.value['body'] = result.value

      next()
    }
  },

  schema: {
    choreSchema: Joi.object().keys({
      name: Joi.string().required(),
      description: Joi.string()
    }),
    // userRegistrationSchema: Joi.object().keys({
    //   profile: Joi.object().keys({
    //     name: Joi.object().keys({
    //       givenName: Joi.string().required(),
    //       familyName: Joi.string().required()
    //     })
    //   }),
    //   email: Joi.string().email().trim(),
    //   password: Joi.string().strip()
    // }),
    userAuthenticationSchema: Joi.object().keys({
      email: Joi.string().email(),
      password: Joi.string().strip()
    })
  }
}

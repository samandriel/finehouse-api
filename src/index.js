import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import morgan from 'morgan'
import routes from './routes'
import passport from 'passport'
import cors from 'cors'
const app = express()

// Database Connection
mongoose.connect(process.env.DB_URI, { useMongoClient: true })
mongoose.Promise = global.Promise

// Middlewares
const corsOptions = {
  origin: 'http://localhost:8080'
}
app.use(cors(corsOptions))
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize())
app.use('/', routes)
app.use((err, req, res, next) => {
  res.status(422).send({error: err.message})
})

// Start server
const port = process.env.PORT || 4000
app.listen(port, () => {
  console.log(`listening for requests on port ${port}`)
})

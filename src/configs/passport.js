import passport from 'passport'
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt'
import { Strategy as FacebookStrategy } from 'passport-facebook'
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth'
import { Strategy as LocalStrategy } from 'passport-local'
import User from './../models/user'

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
  algorithm: 'HS512',
  issuer: 'http://www.finehouse.co/'
}, async (payload, done) => {
  try {
    let user = await User.findById(payload.sub)
    if (!user) {
      return done(null, false)
    }
    return done(null, user)
  } catch (error) {
    done(error, false)
  }
}))

passport.use(new LocalStrategy({
  usernameField: 'email'
}, async (email, password, done) => {
  try {
    const user = await User.findOne({ email })
    if (!user) {
      return done(null, false)
    }
    await user.comparePassword((password), (error, isMatch) => {
      if (error) throw error
      if (!isMatch) {
        return done(null, false)
      }
      done(null, user)
    })
  } catch (error) {
    done(error, false)
  }
}))

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.GOOGLE_CALLBACK_URL
}, async (accessToken, refreshToken, profile, done) => {
  try {
    let user = await User.findOne({ 'google.id': profile.id })
    if (!user) {
      user = new User()
      user.google.id = profile.id
      user.google.token = accessToken
      user.google.email = profile.emails[0].value
      user.name.givenName = profile.name.givenName
      user.name.familyName = profile.name.familyName
      user.email = profile.emails[0].value
      await
      user.save()
      console.log(user)
    }
    return done(null, user)
  } catch (error) {
    done(error, false)
  }
}))

passport.use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: process.env.FACEBOOK_CALLBACK_URL,
  profileFields: ['id', 'name', 'picture', 'email']
}, async (accessToken, refreshToken, profile, done) => {
  try {
    let user = await User.findOne({ 'facebook.id': profile.id })
    if (!user) {
      console.log(profile)
      user = new User()
      user.facebook.id = profile.id
      user.facebook.token = accessToken
      user.facebook.emails = profile.emails[0].value
      user.name.givenName = profile.name.givenName
      user.name.familyName = profile.name.familyName
      user.email = profile.emails[0].value
      await
      user.save()
    }
    return done(null, user)
  } catch (error) {
    done(error, false)
  }
}))

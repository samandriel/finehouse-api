import JWT from 'jsonwebtoken'
import User from './../models/user'
import Family from './../models/family'

function authToken (user) {
  return JWT.sign({
    iss: 'http://www.finehouse.co/',
    sub: user.id,
    exp: Date.now() + 7
  }, process.env.JWT_SECRET, { algorithm: 'HS512' })
}

async function existingAccount (req, res, next) {
  let existingAccount

  try {
    // Check if the new account have an existing account using different authentication method 
    existingAccount = await User.find({}).or([{'google.email': req.body.google.email}, {'facebook.email': req.body.google.email}, {'email': req.body.google.email}])
    existingAccount = existingAccount[0]
    // Check if the another account with the same authentication method is already been in use
    if (existingAccount) {
      if (existingAccount.google.email && existingAccount.google.email !== null) {
        res.status(422).json({ error: `You already connected with another Google Account: ${existingAccount.google.email}, please use this account to login and update your profile` })
      } else {
        existingAccount.update(req.body).exec()
      }
    }
  } catch (err) {
    try {
      // Check if the new account have an existing account using different authentication method 
      existingAccount = await User.find({}).or([{'google.email': req.body.facebook.email}, {'facebook.email': req.body.facebook.email}, {'email': req.body.facebook.email}])
      existingAccount = existingAccount[0]
      console.log(existingAccount)
      // Check if the another account with the same authentication method is already been in use
      if (existingAccount) {
        if (existingAccount.facebook.email && existingAccount.facebook.email !== null) {
          res.status(422).json({ error: `You already connected with another Facebook Account: ${existingAccount.facebook.email}, please use this account to login and update your profile` })
        } else {
          existingAccount.update(req.body).exec()
        }
      }
    } catch (err) {
      try {
        // Check if the new account have an existing account using different authentication method 
        existingAccount = await User.find({}).or([{'google.email': req.body.email}, {'facebook.email': req.body.email}, {email: req.body.email}])
        existingAccount = existingAccount[0]
        // Check if the another account with the same authentication method is already been in use
        console.log(existingAccount)
        if (existingAccount) {
          if (existingAccount.email && existingAccount.email !== null) {
            res.status(422).json({ error: `You already have an account ${existingAccount.email}` })
          } else {
            existingAccount.update(req.body).exec()
          }
        }
      } catch (err) {
        next(err)
      }
    }
  }
  return existingAccount
}

module.exports = {

  registration: async (req, res, next) => {
    try {
      let user
      let family
      // First user?
      User.findOne().exec(async (err, user) => {
        if (err) throw err
        if (!user) {
          user = await User.create(req.body)
          family = await Family.create({ familyName: '', members: [user._id] })
        }
      })

      // Normal operation
      user = await existingAccount(req, res, next)
      if (user) {
        family = await Family.find({members: user._id})
        console.log(JSON.stringify(family))
      } else {
        user = await User.create(req.body)
        try {
          family = await Family.create({ familyName: user.profile.name.familyName, members: [user._id] })
        } catch (err) {
          try {
            family = await Family.create({ familyName: '', members: [user._id] })
          } catch (err) {
            next()
          }
        }
      }
      const token = await authToken(user)
      await res.status(200).json({ token, family })
    } catch (err) {
      await next(err)
    }
  },

  authentication: async (req, res, next) => {
    const token = await authToken(req.user)
    res.status(200).json({ token })
    next()
  }
}

import Reminder from './../models/reminder'

module.exports = {
  index: async (req, res, next) => {
    try {
      await
      res.send({type: 'GET'})
    } catch (error) {
      error(next)
    }
  },

  create: async (req, res, next) => {
    try {
      await
      Reminder.create(req.body).then((chore) => {
        res.send(chore)
      })
    } catch (error) {
      error(next)
    }
  },

  read: async (req, res, next) => {
    try {
      await
      res.send({type: 'GET'})
    } catch (error) {
      error(next)
    }
  },

  update: async (req, res, next) => {
    try {
      await
      res.send({type: 'GET'})
    } catch (error) {
      error(next)
    }
  },

  destroy: async (req, res, next) => {
    try {
      await
      res.send({type: 'GET'})
    } catch (error) {
      error(next)
    }
  }
}
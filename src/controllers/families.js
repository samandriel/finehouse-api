import Family from './../models/family'

module.exports = {
  // create: async (req, res, next) => {
  //   try {
  //     await
  //     Family.create(req.body).then((family) => {
  //       res.send(family)
  //     })
  //   } catch (error) {
  //     error(next)
  //   }
  // },
  read: async (req, res, next) => {
    try {
      console.log(res)
      let family = await Family.findOne({ members: {$in: [req.user._id]} })
      res.status(200).json({ family })
    } catch (error) {
      error(next)
    }
  },

  // update: async (req, res, next) => {
  //   try {
  //     let family = await Family.findOne({ _id: req.body.family._id })
  //     family.update(res.body.family)

  //     await
  //     res.send({type: 'GET'})
  //   } catch (error) {
  //     error(next)
  //   }
  // },

  destroy: async (req, res, next) => {
    try {
      await
      res.send({type: 'GET'})
    } catch (error) {
      error(next)
    }
  }
}

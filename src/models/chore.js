import mongoose, { Schema } from 'mongoose'

const ChoreSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  created_by: Schema.ObjectID
})

const Chore = mongoose.model('Chore', ChoreSchema)
export default Chore

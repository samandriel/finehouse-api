import mongoose, { Schema } from 'mongoose'

const ReminderSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  description: String,
  reminderTime: Date,
  assignedTo: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  checklist: [{
    listItem: String,
    checked: {
      type: Boolean,
      default: false
    }
  }],
  private: {
    type: Boolean,
    defalse: false
  },
  created_by: Schema.ObjectId
})

const Reminder = mongoose.model('Reminder', ReminderSchema)
export default Reminder

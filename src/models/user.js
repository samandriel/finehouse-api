import mongoose, { Schema } from 'mongoose'
import bcrypt from 'bcryptjs'

const UserSchema = new Schema({
  profile: {
    name: {
      givenName: {
        type: String,
        trim: true
      },
      familyName: {
        type: String,
        trim: true
      }
    },
    birthday: {
      type: Date
    },
    picture: {
      type: String
    }
  },
  email: {
    type: String,
    trim: true
  },
  password: String,
  google: {
    id: {
      type: String
    },
    token: {
      type: String
    },
    email: {
      type: String
    }
  },
  facebook: {
    id: {
      type: String
    },
    token: {
      type: String
    },
    email: {
      type: String
    }
  }
})

UserSchema.pre('save', async function (next) {
  try {
    if (this.password != null) {
      const salt = await bcrypt.genSalt(10)
      const hashedPassword = await bcrypt.hash(this.password, salt)
      this.password = hashedPassword
    }
    next()
  } catch (error) {
    next(error)
  }
})

UserSchema.methods.comparePassword = async function (inputPassword, next) {
  try {
    const isMatch = await bcrypt.compare(inputPassword, this.password)
    next(null, isMatch)
  } catch (error) {
    next(error, false)
  }
}

const User = mongoose.model('User', UserSchema)
export default User

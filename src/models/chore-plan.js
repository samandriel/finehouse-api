import mongoose, { Schema } from 'mongoose'

const ChorePlanSchema = new Schema({
  name: {
    type: String,
    unique: true
  },
  days: {
    monday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    tuesday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    wednesday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    thursday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    friday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    saturday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }],
    sunday: [{
      chore_id: {
        type: Schema.ObjectId,
        ref: 'Chore'
      },
      assignTo: [{
        type: Schema.ObjectId,
        ref: 'User'
      }],
      completed: Boolean
    }]
  },
  created_by: Schema.ObjectId
})

const ChorePlan = mongoose.model('ChorePlan', ChorePlanSchema)
export default ChorePlan

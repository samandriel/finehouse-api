import mongoose, { Schema } from 'mongoose'

const FamilySchema = new Schema({
  familyName: {
    type: String
  },
  members: [Schema.ObjectId]
})

const Family = mongoose.model('family', FamilySchema)
export default Family

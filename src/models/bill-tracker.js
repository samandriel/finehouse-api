import mongoose, { Schema } from 'mongoose'

const BillTrackerSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  billCycle: {
    startDate: Number,
    startMonth: Number,
    recurring_payment: String
  },
  reminderTime: Date,
  assignedTo: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  created_by: Schema.ObjectId
})

const BillTracker = mongoose.model('BillTracker', BillTrackerSchema)
export default BillTracker
